# Terraform AWS infra app


This terraform module will create a complete infrastructure on AWS to deploy a basic application with this schema :

* 1 specific VPC
* 1 or more public subnets
* 1 or more private subnets
* 1 or more VMs for the application which will be placed in a public subnet to be reachable over the internet
* 1 or more VMs for the database which will be placed in a private subnet to not be reachable over the internet and to preserve sensitive data
* Configure and generate security groups easily via security groups
   

## Usage

1. Clone the repository

```
git clone https://gitlab.com/Gribhb/terraform-aws-infra-app.git
```

2. Go to the directory

```
cd terraform-aws-infra-app
```

3. Define region and profile to use in the **provider.tf** file

4. Set the differentes variables in the **main.tf** file

```
module "network" {
  source = "./modules/network"

  vpc_cidr           = "<VPC CIDR>"
  vpc_name           = "<NAME OF THE VPC>"
  public_subnets     = ["<CIDR OF THE PUBLIC SUBNET>"] // if you want multiple public subnets ["<CIDR OF THE PUBLIC SUBNET 1>", "<CIDR OF THE PUBLIC SUBNET 2>"]
  private_subnets    = ["<CIDR OF THE PRIVATE SUBNET>"] // if you want multiple private subnets ["<CIDR OF THE PRIVATE SUBNET 1>", "CIDR OF THE PRIVATE SUBNET 2>"]
  availability_zones = ["<AWS REGION>a", "<AWS REGION>b", "<REGION>c"]
  app_ingress_rules = {
    "1" = {
      from_port   = <PORT TO OPEN>
      to_port     = <PORT TO OPEN>
      protocol    = "<PROTOCOL>"
      cidr_blocks = ["<PUBLIC CIDR BLOCK>"] //Warning, if you specify 0.0.0.0/0 it will be open to the world
      description = "<DESCRIPTION>"
    },
    "2" = {
      from_port   = <PORT TO OPEN>
      to_port     = <PORT TO OPEN>
      protocol    = "<PROTOCOL>"
      cidr_blocks = ["<PUBLIC CIDR BLOCK>"] //Warning, if you specify 0.0.0.0/0 it will be open to world
      description = "<DESCRIPTION>"
    }
  }
  db_ingress_rules = {
    "1" = {
      from_port   = <PORT TO OPEN>
      to_port     = <PORT TO OPEN>
      protocol    = "<PROTOCOL>"
      cidr_blocks = ["<PUBLIC SUBNET CIDR BLOCK>"] //To communicate with the application
      description = "<DESCRIPTION>"
    },
    "2" = {
     from_port   = <PORT TO OPEN>
     to_port     = <PORT TO OPEN>
     protocol    = "<PROTOCOL>"
     cidr_blocks = ["<PUBLIC SUBNET CIDR BLOCK>"]
     description = "<DESCRIPTION>"
    }
  }
}


module "instances" {
  source = "./modules/instances"

  number_app_instances = <NUMBER OF APPLICATION VMs>
  ami_app_instances = "<AMI>"
  instance_type_app_instances = "<INSTANCE TYPE>"
  public_subnets = "${element(module.network.public_subnets_id, <NUMBER OF PUBLIC SUBNET>)}" //Warning the 1st subnet will have id 0
  app_security_groups = "${module.network.security_groups_app_ids}"
  ssh_key_app_name = "<NAME OF THE APPLICATION VMs AWS KEY>"
  app_name = "<NAME OF THE APPLICATION VMs>"

  number_db_instances = <NUMBER OF DATABASE VMs>
  ami_db_instances = "<AMI>"
  instance_type_db_instances = "<INSTANCE TYPE>"
  private_subnets = "${element(module.network.private_subnets_id, <NUMBER OF PRIVATE SUBNET>)}" //Warning the 1st subnet will have id 0
  db_security_groups = "${module.network.security_groups_db_ids}"
  ssh_key_db_name = "<NAME OF THE DATABASE VMs AWS KEY>"
  db_name = "<NAME OF THE DATABASE VMs>"
}
```

5. Need to initialize the modules

```
terraform init
```

6. Plan to see what will be creating

```
terraform plan
```

7. Deploy ! :D

```
terraform apply
```

8. To delete all the created resources 

```
terraform destroy
```


## Example

```
module "network" {
  source = "./modules/network"

  vpc_cidr           = "10.0.0.0/16"
  vpc_name           = "vpc-module-test"
  public_subnets     = ["10.0.1.0/24"]
  private_subnets    = ["10.0.10.0/24"]
  availability_zones = ["eu-west-3a", "eu-west-3b", "eu-west-3c"]
  app_ingress_rules = {
    "1" = {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      description = "HTTP"
    },
    "2" = {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = ["0.0.0.0/0"]
      description = "SSH"
    }
  }
  db_ingress_rules = {
    "1" = {
      from_port   = 3306
      to_port     = 3306
      protocol    = "tcp"
      cidr_blocks = ["10.0.1.0/24"]
      description = "DATABASE"
    },
    "2" = {
     from_port   = 22
     to_port     = 22
     protocol    = "tcp"
     cidr_blocks = ["10.0.1.0/24"]
     description = "SSH"
    }
  }
}


module "instances" {
  source = "./modules/instances"

  number_app_instances = 1
  ami_app_instances = "ami-01d14e0ab732be0e4"
  instance_type_app_instances = "t2.micro"
  public_subnets = "${element(module.network.public_subnets_id, 0)}"
  app_security_groups = "${module.network.security_groups_app_ids}"
  ssh_key_app_name = "app_sshtestmodule"
  app_name = "mytestapp"

  number_db_instances = 1
  ami_db_instances = "ami-01d14e0ab732be0e4"
  instance_type_db_instances = "t2.micro"
  private_subnets = "${element(module.network.private_subnets_id, 0)}"
  db_security_groups = "${module.network.security_groups_db_ids}"
  ssh_key_db_name = "db_sshtestmodule"
  db_name = "mytestdb"
}
```


## Troubleshooting

Sometimes, during the first execution of the module, it will be possible to get this error :

```
Error: Error launching source instance: InvalidKeyPair.NotFound: The key pair 'app_sshtestmodule' does not exist
      status code: 400, request id: af39e2e2-0b89-4022-9b48-t12u4ca0443w

  with module.instances.aws_instance.app_instances[0],
  on modules/instances/main.tf line 1, in resource "aws_instance" "app_instances":
   1: resource "aws_instance" "app_instances" {
```

Just run again the apply command

```
terraform apply
```


*Feel free to contact me on twitter (@gribhb) if needed or submit ameliorations*
