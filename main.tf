module "network" {
  source = "./modules/network"

  vpc_cidr           = "<VPC CIDR>"
  vpc_name           = "<NAME OF THE VPC>"
  public_subnets     = ["<CIDR OF THE PUBLIC SUBNET>"]
  private_subnets    = ["<CIDR OF THE PRIVATE SUBNET>"]
  availability_zones = ["<AWS REGION>a", "<AWS REGION>b", "<AWS REGION>c"]
  app_ingress_rules = {
    "1" = {
      from_port   = <PORT TO OPEN>
      to_port     = <PORT TO OPEN>
      protocol    = "<PROTOCOL>"
      cidr_blocks = ["<PUBLIC CIDR BLOCK>"]
      description = "<DESCRIPTION>"
    },
    "2" = {
      from_port   = <PORT TO OPEN>
      to_port     = <PORT TO OPEN>
      protocol    = "<PROTOCOL>"
      cidr_blocks = ["<PUBLIC CIDR BLOCK>"]
      description = "<DESCRIPTION>"
    }
  }
  db_ingress_rules = {
    "1" = {
      from_port   = <PORT TO OPEN>
      to_port     = <PORT TO OPEN>
      protocol    = "<PROTOCOL>"
      cidr_blocks = ["<PUBLIC SUBNET CIDR BLOCK>"]
      description = "<DESCRIPTION>"
    },
    "2" = {
     from_port   = <PORT TO OPEN>
     to_port     = <PORT TO OPEN>
     protocol    = "<PORTOCOL>"
     cidr_blocks = ["<PUBLIC SUBNET CIDR BLOCK>"]
     description = "<DESCRIPTION>"
    }
  }
}


module "instances" {
  source = "./modules/instances"

  number_app_instances = <NUMBER OF APPLICATION VMs>
  ami_app_instances = "<AMI>"
  instance_type_app_instances = "<INSTANCE TYPE>"
  public_subnets = "${element(module.network.public_subnets_id, <NUMBER OF PUBLIC SUBNET>)}"
  app_security_groups = "${module.network.security_groups_app_ids}"
  ssh_key_app_name = "<NAME OF THE APPLICATION VMs AWS KEY>"
  app_name = "<NAME OF THE APPLICATION VMs>"

  number_db_instances = <NUMBER OF DATABASE VMs>
  ami_db_instances = "<AMI>"
  instance_type_db_instances = "<INSTANCE TYPE>"
  private_subnets = "${element(module.network.private_subnets_id, <NUMBER OD PRIVATE SUBNET>)}"
  db_security_groups = "${module.network.security_groups_db_ids}"
  ssh_key_db_name = "<NAME OF THE DATABASE VMs AWS KEY>"
  db_name = "<NAME OF THE DATABASE VMs>"
}
