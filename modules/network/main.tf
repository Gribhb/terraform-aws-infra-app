resource "aws_vpc" "vpc" {
  cidr_block = "${var.vpc_cidr}"
  enable_dns_hostnames = true
  tags = {
    Name = "${var.vpc_name}-vpc"
  }
}


resource "aws_internet_gateway" "internet_gateway" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags = {
    Name = "${var.vpc_name}-internet_gateway"
  }
} 


//resource "aws_eip" "nat_elastic_ip" {
//  vpc = true
//  depends_on = [aws_internet_gateway.internet_gateway]
//}


resource "aws_nat_gateway" "nat_gateway" {
//allocation_id = "${aws_eip.nat_elastic_ip.id}"
  connectivity_type = "private"
  subnet_id = "${element(aws_subnet.public_subnet.*.id, 0)}"
  depends_on = [aws_internet_gateway.internet_gateway]
  tags = {
    Name = "nat_gateway"
  }
}


resource "aws_subnet" "public_subnet" {
  vpc_id = "${aws_vpc.vpc.id}"
  count = "${length(var.public_subnets)}"
  cidr_block = "${element(var.public_subnets, count.index)}"
  availability_zone = "${element(var.availability_zones, count.index)}"
  map_public_ip_on_launch = true
  tags = {
    Name = "${var.vpc_name}-${element(var.availability_zones, count.index)}-public"
  }
}


resource "aws_subnet" "private_subnet" {
  vpc_id = "${aws_vpc.vpc.id}"
  count = "${length(var.private_subnets)}"
  cidr_block = "${element(var.private_subnets, count.index)}"
  availability_zone = "${element(var.availability_zones, count.index)}"
  tags = {
    Name = "${var.vpc_name}-${element(var.availability_zones, count.index)}-private"
  }
}


resource "aws_route_table" "public_route_table" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags = {
    Name = "${var.vpc_name}-public-route-table"
  }
}


resource "aws_route_table" "private_route_table" {
  vpc_id = "${aws_vpc.vpc.id}"
  tags = {
    Name = "${var.vpc_name}-private-route-table"
  }
}


resource "aws_route" "internet_gateway_public_subnet" {
  route_table_id = "${aws_route_table.public_route_table.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.internet_gateway.id}"
}


resource "aws_route" "nat_gateway_private_subnet" {
  route_table_id = "${aws_route_table.private_route_table.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id = "${aws_nat_gateway.nat_gateway.id}"
}


resource "aws_route_table_association" "route_table_association_public_subnets" {
  count = "${length(var.public_subnets)}"
  subnet_id = "${element(aws_subnet.public_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.public_route_table.id}"
}


resource "aws_route_table_association" "route_table_association_private_subnets" {
  count = "${length(var.private_subnets)}"
  subnet_id = "${element(aws_subnet.private_subnet.*.id, count.index)}"
  route_table_id = "${aws_route_table.private_route_table.id}"
}


resource "aws_security_group" "app_security_group" {
  name = "${var.vpc_name}-app-sg"
  description = "Security group to apply to app servers"
  vpc_id = "${aws_vpc.vpc.id}"
  depends_on = [aws_vpc.vpc]
  
  dynamic "ingress" {
    for_each = var.app_ingress_rules
    content {
      from_port = ingress.value.from_port
      to_port = ingress.value.to_port
      protocol = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
      description = ingress.value.description
    }
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"] 
  }
  tags = {
    Name = "${var.vpc_name}"
  }
}

resource "aws_security_group" "db_security_group" {
  name = "${var.vpc_name}-db-sg"
  description = "Security group to apply to db servers"
  vpc_id = "${aws_vpc.vpc.id}"
  depends_on = [aws_vpc.vpc]
  
  dynamic "ingress" {
    for_each = var.db_ingress_rules
    content {
      from_port = ingress.value.from_port
      to_port = ingress.value.to_port
      protocol = ingress.value.protocol
      cidr_blocks = ingress.value.cidr_blocks
      description = ingress.value.description
    }
  }
  egress {
    from_port = 0
    to_port = 0
    protocol = -1
    cidr_blocks = ["0.0.0.0/0"] 
  }
  tags = {
    Name = "${var.vpc_name}"
  }
}
