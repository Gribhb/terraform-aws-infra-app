variable "vpc_cidr" {
  description =  "Cidr of the VPC"
}

variable "vpc_name" {
  description = "Name of the VPC"
}

variable "public_subnets" {
  type = list
  description = "Cidr block of the public subnet"
}

variable "private_subnets" {
  type = list
  description = "Cidr block of the private subnet"
}

variable "availability_zones" {
  type = list
  description = "AZ for the instances will be launched"
}

variable "app_ingress_rules" {
  description = "Ingress security group rules for app servers"
  type = map
}

variable "db_ingress_rules" {
  description = "Ingress security group rules for db servers"
  type = map
}
