output "vpc_id" {
  value = "${aws_vpc.vpc.id}"
}

output "public_subnets_id" {
  value = "${aws_subnet.public_subnet.*.id}"
}

output "private_subnets_id" {
  value = "${aws_subnet.private_subnet.*.id}"
}

output "security_groups_app_ids" {
  value = "${aws_security_group.app_security_group.id}"
}

output "security_groups_db_ids" {
  value = "${aws_security_group.db_security_group.id}"
}
