resource "aws_instance" "app_instances" {
  count = "${var.number_app_instances}"
  ami = "${var.ami_app_instances}"
  associate_public_ip_address = true
  instance_type = "${var.instance_type_app_instances}"
  key_name = "${var.ssh_key_app_name}"
  subnet_id = "${var.public_subnets}"
  vpc_security_group_ids = ["${var.app_security_groups}"]

  tags = {
    Name = "${var.app_name}-${count.index}"
  }
}

resource "aws_instance" "db_instances" {
  count = "${var.number_db_instances}"
  ami = "${var.ami_db_instances}"
  associate_public_ip_address = false
  instance_type = "${var.instance_type_db_instances}"
  key_name = "${var.ssh_key_db_name}"
  subnet_id = "${var.private_subnets}"
  vpc_security_group_ids = ["${var.db_security_groups}"]

  tags = {
    Name = "${var.db_name}-${count.index}"
  }
}


resource "tls_private_key" "app_ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
} 

resource "tls_private_key" "db_ssh_key" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "aws_key_pair" "instances_app_generated_key_pair" {
  key_name = "${var.ssh_key_app_name}"
  public_key = tls_private_key.app_ssh_key.public_key_openssh

  provisioner "local-exec" {
    command = <<-EOF
      echo '${tls_private_key.app_ssh_key.private_key_pem}' > ./'${var.ssh_key_app_name}'.pem
      chmod 400 ./'${var.ssh_key_app_name}'.pem
    EOF
  }
}

resource "aws_key_pair" "instances_db_generated_key_pair" {
  key_name = "${var.ssh_key_db_name}"
  public_key = tls_private_key.db_ssh_key.public_key_openssh

  provisioner "local-exec" {
    command = <<-EOF
      echo '${tls_private_key.db_ssh_key.private_key_pem}' > ./'${var.ssh_key_db_name}'.pem
      chmod 400 ./'${var.ssh_key_db_name}'.pem
    EOF
  }
}
