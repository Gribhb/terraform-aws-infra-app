variable "ami_app_instances" {
  type = string
}

variable "instance_type_app_instances" {
  type = string
}

variable "public_subnets" {
  type = string
}

variable "app_security_groups" {
  type = string
}

variable "app_name" {
  type = string
}

variable "ami_db_instances" {
  type = string
}

variable "instance_type_db_instances" {
  type = string
}

variable "private_subnets" {
  type = string
}

variable "db_security_groups" {
  type = string
}

variable "db_name" {
  type = string
}

variable "number_app_instances" {
  type = number
}

variable "number_db_instances" {
  type = number
}

variable "ssh_key_app_name" {
  type = string
}

variable "ssh_key_db_name" {
  type = string
}
